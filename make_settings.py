#!/usr/bin/env python

from lxml import etree
import json

schemalist = etree.Element('schemalist')
schemalist.set('gettext-domain', 'gnome-shell-extensions-TodoTxt')
schema = etree.Element('schema', path='/org/gnome/shell/extensions/TodoTxt/', id='org.gnome.shell.extensions.TodoTxt')
schemalist.append(schema)

json_data = json.load(open('settings.json'))


def getXmlType(jsonKey):
    jsonType = jsonKey['type']
    if jsonType == 'boolean':
        return "b"
    if (jsonType == 'path') or (jsonType == 'string'):
        return "s"
    if jsonType == 'integer':
        return "i"
    if jsonType == 'dictionary':
        return jsonKey['signature']
    if jsonType == 'shortcut':
        return 'as'
    return "x"

for key in json_data.keys():
    keyElement = etree.Element('key', name=key, type=getXmlType(json_data[key]))
    defaultElement = etree.Element('default')
    if 'default_value' in json_data[key]:
        text = json_data[key]['default_value']
        if getXmlType(json_data[key]) == 's':
            text = '"{0}"'.format(text)
        if json_data[key]['type'] == 'shortcut':
            text = etree.CDATA(text)
        defaultElement.text = text

    keyElement.append(defaultElement)

    summaryElement = etree.Element('summary')
    if 'summary' in json_data[key]:
        summaryElement.text = json_data[key]['summary']
    keyElement.append(summaryElement)

    schema.append(keyElement)

print etree.tostring(schemalist, pretty_print=True, xml_declaration=True, encoding='UTF-8')
